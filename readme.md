[KITTY](https://sw.kovidgoyal.net/kitty/) caps test

## How to use
Step 1. Enable echo:// support in KITTY

Put those lines in `~/.config/kitty/open-actions.conf`
```
protocol echo
action send_text all ${FRAGMENT}\n
```

Step 2. Run the app in KITTY
```
zig build run
```

Step 3. Click the links

You should see something like this
```
All your codebase are belong to us.
Please choose: (red pill/blue pill)
> #red
Read: #red
```

## Caveat
Leading slash in URI is mandatory (for now).
