const std = @import("std");

pub fn sethyperlink(comptime uri: []const u8) []const u8 {
    return "\x1b]8;;" ++ uri ++ "\x1b\\";
}

pub fn hyperlinked(comptime content: []const u8, comptime link: []const u8) []const u8 {
    return sethyperlink("echo:#" ++ link) ++ content ++ sethyperlink("");
}

pub fn main() !void {
    // Prints to stderr (it's a shortcut based on `std.io.getStdErr()`)
    std.debug.print("All your {s} are belong to us.\n", .{"codebase"});

    // stdout is for the actual output of your application, for example if you
    // are implementing gzip, then only the compressed bytes should be sent to
    // stdout, not any debugging messages.
    const stdin = std.io.getStdIn().reader();
    const stdout = std.io.getStdOut().writer();
    try main_r(stdin, stdout);
}

pub fn main_r(fin: std.fs.File.Reader, fout: std.fs.File.Writer) !void {
    var buffer: [128]u8 = undefined;
    
    try fout.print("Please choose: ({s}/{s})\n> ", .{hyperlinked("red pill", "#red"), hyperlinked("blue pill", "#blue")});    

    const line = try fin.readUntilDelimiter(&buffer, '\n');
    try fout.print("Read: {s}\n", .{line});
}

test "simple test" {
    var list = std.ArrayList(i32).init(std.testing.allocator);
    defer list.deinit(); // try commenting this out and see if zig detects the memory leak!
    try list.append(42);
    try std.testing.expectEqual(@as(i32, 42), list.pop());
}
